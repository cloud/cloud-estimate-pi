#!/usr/bin/env bash

# Pi estimation container entrypoint
# Usage:
# * executing explicit command:     entrypoint.sh <command-to-execute>
# * executing named Pi estimation:  PI_ESTIMATION_MODE=pi_monte_carlo entrypoint.sh
# 
# Notes:
# * either <command-to-execute> or valid env. variable PI_ESTIMATION_MODE should be supplied
# * valid PI_ESTIMATION_MODE values are:
#   - PI_ESTIMATION_MODE=pi_monte_carlo
#   - PI_ESTIMATION_MODE=pi_monte_carlo.py
#   - PI_ESTIMATION_MODE=pi_gather_scatter


set -eo pipefail

[[ "${PI_ESTIMATION_TRACE}" =~ ^1|[Tt]rue$ ]] && \
  set -x

# constants
SCRIPT_FILE=$(readlink -f $0)
SCRIPT_DIR=$(dirname ${SCRIPT_FILE})

declare -A PI_ESTIMATION_MODES
PI_ESTIMATION_MODES[pi_monte_carlo]="${SCRIPT_DIR}/pi_monte_carlo"
PI_ESTIMATION_MODES[pi_monte_carlo.cpp]="${PI_ESTIMATION_MODES[pi_monte_carlo]}"
PI_ESTIMATION_MODES[pi_monte_carlo.py]="${SCRIPT_DIR}/pi_monte_carlo.py"
PI_ESTIMATION_MODES[pi_gather_scatter]="/usr/lib64/openmpi/bin/mpirun --allow-run-as-root -n 4 ${SCRIPT_DIR}/pi_gather_scatter"
PI_ESTIMATION_MODES[pi_gather_scatter.c]="${PI_ESTIMATION_MODES[pi_gather_scatter]}"

# help
if [ -z "${PI_ESTIMATION_MODE}" -a "$#" == 0 ]; then
    grep -E "^# " ${SCRIPT_FILE} | sed 's/^# /  /g'
    exit 1
fi

# execute the command
if [[ " ${!PI_ESTIMATION_MODES[*]} " =~ [[:space:]]${PI_ESTIMATION_MODE}[[:space:]] ]]; then
    exec ${PI_ESTIMATION_MODES["${PI_ESTIMATION_MODE}"]}
else
    exec "$@"
fi
