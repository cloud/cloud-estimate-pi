FROM centos:7

RUN mkdir -p /workplace
COPY pi_* Dockerfile entrypoint.sh dependencies.*.txt /workplace/
WORKDIR /workplace
RUN yum -y install $(cat dependencies.yum.txt) && \
    yum clean all

ENTRYPOINT ["/workplace/entrypoint.sh"]

ARG VERSION=unknown-version
ARG BUILD_DATE=unknown-date
ARG CI_COMMIT_SHA=unknown
ARG CI_BUILD_HOSTNAME
ARG CI_BUILD_JOB_NAME
ARG CI_BUILD_ID

LABEL maintainer="" \
      org.label-schema.schema-version="1.0.0-rc.1" \
      org.label-schema.vendor="Masaryk University, ICS" \
      org.label-schema.name="cloud-estimate-pi" \
      org.label-schema.version="$VERSION" \
      org.label-schema.build-date="$BUILD_DATE" \
      org.label-schema.build-ci-job-name="$CI_BUILD_JOB_NAME" \
      org.label-schema.build-ci-build-id="$CI_BUILD_ID" \
      org.label-schema.build-ci-host-name="$CI_BUILD_HOSTNAME" \
      org.label-schema.url="https://gitlab.ics.muni.cz/cloud/cloud-estimate-pi" \
      org.label-schema.vcs-url="https://gitlab.ics.muni.cz/cloud/cloud-estimate-pi" \
      org.label-schema.vcs-ref="$CI_COMMIT_SHA"
