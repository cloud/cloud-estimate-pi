# VM based cloud automation

It is often seen that cloud users are performing manual actions again and again.

[`Ansible` directory](/ansible/) shows how to declaratively work with VM based cloud.

## Workflow

There are three basic actions you want to do with cloud:
 * initialization
   * preparation of the cloud resources so they can be used for particular use
 * deployment and project execution
   * deployment of all necessary data to cloud resources (VMs)
   * execution of the project application[s]
 * download of the data
   * collection of the data (logs) from cloud resource[s]
 * destroy of the environment
   * uninstall packages
   * project dir clean-up


## Workflow implementations

### working with containers [`cloud-project-container`](cloud-project-container.yml) playbook

The typical steps are following:
 * develop your project
   * create merge request
   * approve & merge merge request
 * release your project via pushing the semver compatible tag ([example](https://gitlab.ics.muni.cz/cloud/cloud-estimate-pi/-/tags/v1.1.6))
   * CI pipeline creates container image and pushes your application[s] to (public) container registry
 * `option A.` execute project application[s] from project CI pipeline (manually) [triggering the deploy stage](https://gitlab.ics.muni.cz/cloud/cloud-estimate-pi/-/jobs/244008)
   * application execution logs is stored in CI job artifacts
 * `option B.` execute project application[s] in the cloud manually
   * [install ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-ansible-on-specific-operating-systems) for your system
   * execute above listed workflow actions
```sh
# terminal A: (optional)
# create tunnel to internal cloud network (if your VM does not have public ip address)
sshuttle -r centos@<centos-jump-host> 172.16.0.0/22
```

```sh
# terminal B:
# initialize cloud resources
ansible-playbook  -i ansible_hosts.yml -t init     ./cloud-project-container.yml
# deploy & execute in the cloud
ansible-playbook  -i ansible_hosts.yml -t deploy   ./cloud-project-container.yml
# download (generated) data from the cloud (downloads stored under [`ansible/logs` directory](/ansible/logs))
ansible-playbook  -i ansible_hosts.yml -t download ./cloud-project-container.yml
# destroy the environment
ansible-playbook  -i ansible_hosts.yml -t destroy  ./cloud-project-container.yml
```

### working with binaries

The typical steps are following:
 * develop your project
   * create merge request
   * approve & merge merge request
 * execute project application[s] in the cloud manually
   * [install ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-ansible-on-specific-operating-systems) for your system
   * execute above listed workflow actions (binaries will be built in the cloud)
```sh
# terminal A: (optional)
# create tunnel to internal cloud network (if your VM does not have public ip address)
sshuttle -r centos@<centos-jump-host> 172.16.0.0/22
```

```sh
# terminal B:
# initialize cloud resources
ansible-playbook  -i ansible_hosts.yml -t init     ./cloud-project-native.yml
# deploy & execute in the cloud
ansible-playbook  -i ansible_hosts.yml -t deploy   ./cloud-project-native.yml
# download (generated) data from the cloud (downloads stored under [`ansible/logs` directory](/ansible/logs))
ansible-playbook  -i ansible_hosts.yml -t download ./cloud-project-native.yml
# destroy the environment
ansible-playbook  -i ansible_hosts.yml -t destroy  ./cloud-project-native.yml
```
