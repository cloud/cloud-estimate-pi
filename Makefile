# Main makefile

# constants
MAKEFILE_FILE          := $(abspath $(lastword $(MAKEFILE_LIST)))
MAKEFILE_DIR           := $(patsubst %/,%, $(dir $(MAKEFILE_FILE)))

# targets
.PHONY: clean

all: help                                                   # default target
	$(error specify makefile target!)

help:                                                       # this help summary
	@echo -e "Available makefile targets are:\n"
	@grep -E "^[a-zA-Z0-9_-]+:" $(MAKEFILE_FILE)

clean:                                                      # clean all binaries
	rm -f $(MAKEFILE_DIR)/pi_gather_scatter $(MAKEFILE_DIR)/pi_monte_carlo $(MAKEFILE_DIR)/pi_monte_carlo.py

build: pi_gather_scatter pi_monte_carlo pi_monte_carlo.py   # build all binaries

rebuild: clean build

pi_gather_scatter:                                          # build pi_gather_scatter
	/usr/lib64/openmpi/bin/mpicc $(MAKEFILE_DIR)/src/pi_gather_scatter.c -o pi_gather_scatter

pi_monte_carlo:                                             # build pi_monte_carlo
	g++ $(MAKEFILE_DIR)/src/pi_monte_carlo.cxx -o pi_monte_carlo

pi_monte_carlo.py:                                          # make pi_monte_carlo.py executable
	cp $(MAKEFILE_DIR)/src/pi_monte_carlo.py pi_monte_carlo.py
	chmod +x pi_monte_carlo.py

