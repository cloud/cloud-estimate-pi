# Cloud Estimate Pi

Cloud Estimate Pi project shares best practice approaches how to work with the MetaCentrum cloud.

# Prerequisites
* [VCS knowledge](https://en.wikipedia.org/wiki/Version_control), [preferably GIT](https://githowto.com)
* [Container and container runtime basics](https://docker-curriculum.com/)
* [CI/CD basic knowledge](https://docs.gitlab.com/ee/ci/introduction)
* [Cloud overview](https://en.wikipedia.org/wiki/Cloud_computing)
* [Ansible basics](https://medium.com/@denot/ansible-101-d6dc9f86df0a)
* [Software engineering basics](https://www.guru99.com/software-engineering-tutorial.html)

# What project performs and how to reuse

Project estimates [Pi value](https://en.wikipedia.org/wiki/Pi) using three different programs stored in [`src/` subdirectory](src/).

To reuse such project structure you want to:
 * put your code in the [src/](src/) directory
 * update [`Makefile` `build*` targets](Makefile) so your application builds (including `dependencies.*.txt` files)
 * update [`Dockerfile`](Dockerfile) and [`entrypoint.sh`](entrypoint.sh) to install applications in the container
 * reuse one of the ansible workflow playbooks either `cloud-project-container` or `cloud-project-native`
   * update [`ansible/ansible_hosts.yml`](ansible/ansible_hosts.yml) with your cloud resource IP(s).

# Recommended approaches (best-practices)

## Repository structure

Standardized git repository structure:
 * [`conf/`](conf/) directory includes all configuration files
 * [`src/`](src/) directory should contain application source codes
 * [`kubernetes/`](kubernetes/) directory should contain kubernetes manifests
 * there is building script or makefile in the root of the project ([`Makefile`](Makefile))
 * there are CI jobs defined and passing ([`.gitlab-ci.yaml`](.gitlab-ci.yaml))
   * scripts used in CI pipeline should come from CI container image, project specific scripts is in [`ci/`](ci/) directory
 * License should be present in the root of the project ([`LICENSE`](LICENSE))
 * It is recommended to package application into container ([`Dockerfile`](Dockerfile))

## How to build applications (in CI)

* Application build should be predictable and fully [automated in the CI process](https://gitlab.ics.muni.cz/cloud/cloud-estimate-pi/-/blob/4c09c062f220631bc6357eef38612e3aa07435da/.gitlab-ci.yml#L17-80).
* Application release should be triggered by git tag (example release [`v1.1.6`](https://gitlab.ics.muni.cz/cloud/cloud-estimate-pi/-/tags/v1.1.6))
  * application version should follow [semantic versioning v2](https://semver.org/)
* CI should contain application tests
* Application distribution options (sorted by best-practice)
  * as a container image
    * best for predictability, portability and accessibility
    * application container is defined via [`Dockerfile`](Dockerfile)
  * as a OS dependent package
  * as a binary
    * built as CI job artifact ([example](https://gitlab.ics.muni.cz/cloud/cloud-estimate-pi/-/jobs/244008))


## How to use application in VM based cloud (OpenStack)

Define set of automation scripts based on automation framework (ansible, terraform, ...) which perform:
  * cloud resource initialization
  * application deployment and execution
  * download of the data

[Read the details](ansible/README.md).


## How to interact with container based cloud (Kubernetes)

TODO
