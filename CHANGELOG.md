# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## [1.1.7] - 2021-04-14
### Changed
- ansible workflow now uses tags
- documentation improved (prerequisities added)

## [1.1.6] - 2021-04-07
### Fixed
- Project directory structure contain suggested conf/ and kubernetes/ subdirectories

## [1.1.5] - 2021-04-07
### Changed
- Project migrated to public place

## [1.1.4] - 2021-03-26
### Fixed
- CI/CD supports vX.Y.Z tags too

## [1.1.3] - 2021-03-26
### Changed
- project could be tagged in git with vX.Y.Z or X.Y.Z

## [1.1.1] - 2021-03-24
### Fixed
- pi_gather_scatter pi estimator is able to run completely inside the container

## [1.1.0] - 2021-03-24
### Added
- Add ansible playbooks
- Added deban dependency packages
### Fixed
- src/pi_monte_carlo.py shebang fixed

## [1.0.1] - 2021-03-24
### Changed
- Move container image to quay.io (container image pull with no authorization)

## [1.0.0] - 2021-03-23
### Added
- Initial release
